/*******************************************************************************
*
* Copyright (C) 2019 TUSUR, FB, Gaifullina Anastasia
*
* File              : main.cpp
* Compiler          : IAR Embedded Workbench for ARM 8.32.3
* Version           : 1.0
* Created File      : 26.02.2019
* Last modified     : 11.03.2019
*
* Support mail      : 
* Target MCU        : MCU: Milandr 1986BE9x
*
* Description       : 
*
********************************************************************************/
#include <iostream>
#include <cstring>
#include <time.h>
#include <stdio.h>
#include <ctime>
template <typename T>
class Mass
{
public:
	    T *Ptr;
	    const int size;
		T *array;
		T summ;
		int top;
		bool Push(const T );
		void Print();
		Mass(int =10);

};


template <typename T>
Mass<T>::Mass(int maxSize) :

	size(maxSize) 
	{
		array = new T[size]; 
		top = 0;
	}
	

template <typename T>
bool Mass<T>::Push(const T value)
{
	array[top] = value;
	top++;
	return true;
}

template <typename T>
void Mass<T>::Print()
{
	array[size-1] = 0;
	for (int ix = 0; ix < size-1; ix++)
	{	
		array[size-1] += array[ix];
		
	}
	printf("%i\n",array[size-1]);
}


int main(int argc, char *argv[])
{
	
	Mass <int> my(5);
        int temp;
        srand( time( 0 ) );
	printf("element : \n");
	int ct = 0;
	while (ct++ != 5)
	{       
		temp=rand() % 10;
                printf("%i\n",temp);
		my.Push(temp);
	}
	my.Print(); 
	return 0;
}